# 2022 PJJ SQL
***
## Hari Selasa, 8 Juni 2021
* Pengenalan SQL
* Latihan 1
* Normalisasi
* Kunci Jawaban Latihan Normalisasi

## Hari Rabu, 9 Juni 2021
* Pengenalan SQL (Lanjutan)
* Tugas Basic Query
* FUNCTION
* Tugas FUNCTION

## Hari Kamis, 10 Juni 2021
* Export Hasil Query
* Tugas Export Hasil Query
* Join-Union
* Tugas Join
* Subquery
* Tugas Subquery

## Hari Jumat, 11 Juni 2021
* Studi Kasus Komprehensif