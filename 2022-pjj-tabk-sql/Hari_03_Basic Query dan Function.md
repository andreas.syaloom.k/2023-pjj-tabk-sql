# Tugas Basic Query
***
Tugas basic select + argument (group, order, where)
1. **Dapatkan informasi 10 pegawai yang memiliki gaji paling tinggi.**
<pre><code>SELECT TOP 10 FIRST_NAME , LAST_NAME , SALARY FROM EMPLOYEES
ORDER BY SALARY DESC 
</pre></code>
**Output:**
| FIRST\_NAME | LAST\_NAME | SALARY  |
| ----------- | ---------- | ------- |
| Mozhe       | Atkinson   | 99000.0 |
| Steven      | King       | 24000.0 |
| Lex         | De Haan    | 17000.0 |
| Neena       | Kochhar    | 17000.0 |
| John        | Russell    | 14000.0 |
| Karen       | Partners   | 13500.0 |
| Michael     | Hartstein  | 13000.0 |
| Nancy       | Greenberg  | 12008.0 |
| Shelley     | Higgins    | 12008.0 |
| Alberto     | Errazuriz  | 12000.0 |

2. **Dapatkan informasi 3 orang pegawai dengan JOB_ID berakhiran dengan “_CLERK” dengan gaji tertinggi.** 

<pre><code>SELECT TOP 3 FIRST_NAME, LAST_NAME, JOB_ID, SALARY FROM EMPLOYEES 
WHERE SUBSTRING(JOB_ID, 4,8) = 'CLERK'
ORDER BY SALARY DESC</pre></code>

**Output:**
| FIRST\_NAME | LAST\_NAME | JOB\_ID   | SALARY |
| ----------- | ---------- | --------- | ------ |
| Mozhe       | Atkinson   | ST\_CLERK | 99000  |
| Nandita     | Sarchand   | SH\_CLERK | 4200   |
| Alexis      | Bull       | SH\_CLERK | 4100   |

3. **Buatlah laporan jumlah pegawai dan jumlah gaji dari tiap departemen, dan diurutkan dari departemen dengan jumlah pegawai terbanyak.**
<pre><code>SELECT DEPARTMENT_ID, COUNT(EMPLOYEE_ID) AS JUMLAH_PEGAWAI, SUM(SALARY) AS JUMLAH_GAJI FROM EMPLOYEES 
GROUP BY DEPARTMENT_ID
ORDER BY JUMLAH_PEGAWAI DESC</pre></code>
**Output:**
| FIRST\_NAME | LAST\_NAME | SALARY  |
| ----------- | ---------- | ------- |
| Mozhe       | Atkinson   | 99000.0 |
| Steven      | King       | 24000.0 |
| Lex         | De Haan    | 17000.0 |
| Neena       | Kochhar    | 17000.0 |
| John        | Russell    | 14000.0 |
| Karen       | Partners   | 13500.0 |
| Michael     | Hartstein  | 13000.0 |
| Nancy       | Greenberg  | 12008.0 |
| Shelley     | Higgins    | 12008.0 |
| Alberto     | Errazuriz  | 12000.0 |

4. **Dari tabel JOBS, Dapatkan JOB_TITLE dengan range gaji paling besar?**
<pre><code>SELECT JOB_ID, JOB_TITLE, (MAX_SALARY-MIN_SALARY) AS RANGE_SALARY
FROM JOBS
ORDER BY RANGE_SALARY DESC</pre></code>
**Output:**
| JOB\_ID  | JOB\_TITLE                    | RANGE\_SALARY |
| -------- | ----------------------------- | ------------- |
| AD\_PRES | President                     | 19920         |
| AD\_VP   | Administration Vice President | 15000         |
| SA\_MAN  | Sales Manager                 | 10080         |
| AC\_MGR  | Accounting Manager            | 7800          |
| FI\_MGR  | Finance Manager               | 7800          |
| PU\_MAN  | Purchasing Manager            | 7000          |
| SA\_REP  | Sales Representative          | 6008          |

5. **Lakukan pengujian apakah terdapat pegawai selain SA_REP dan SA_MAN yang mendapatkan komisi**
<pre><code>
SELECT*FROM EMPLOYEES 
WHERE JOB_ID!='SA_MAN' AND JOB_ID!='SA_REP' AND COMMISSION_PCT>0
</pre></code>
**Output:**
| EMPLOYEE\_ID | FIRST\_NAME | LAST\_NAME | EMAIL  | PHONE\_NUMBER | HIRE\_DATE | JOB\_ID   | SALARY | COMMISSION\_PCT | MANAGER\_ID | DEPARTMENT\_ID |
| ------------ | ----------- | ---------- | ------ | ------------- | ---------- | --------- | ------ | --------------- | ----------- | -------------- |
| 190          | Timothy     | Gates      | TGATES | 650.505.3876  | 11/07/2006 | SH\_CLERK | 2900   | 1               | 122         | 50             |


6. **Lakukan pengujian apakah terdapat pegawai yang tidak memiliki department.**
<pre><code>SELECT * FROM EMPLOYEES WHERE DEPARTMENT_ID IS NULL</pre></code>
**Output:**
| EMPLOYEE\_ID | FIRST\_NAME | LAST\_NAME | EMAIL  | PHONE\_NUMBER      | HIRE\_DATE | JOB\_ID | SALARY | COMMISSION\_PCT | MANAGER\_ID | DEPARTMENT\_ID |
| ------------ | ----------- | ---------- | ------ | ------------------ | ---------- | ------- | ------ | --------------- | ----------- | -------------- |
| 178          | Kimberely   | Grant      | KGRANT | 011.44.1644.429263 | 24/05/2007   | SA\_REP | 7000.0 | 15.0            | 149         |                | 

7. **Dapatkan informasi 10 pegawai yang memiliki masa kerja paling lama, dan berapa lama masa kerja pegawai tersebut. HINT: masa kerja dapat menggunakan fungsi DATEDIFF**
<pre><code>SELECT TOP 10 FIRST_NAME, LAST_NAME, HIRE_DATE, 
DATEDIFF(YEAR, HIRE_DATE, GETDATE()) AS MASA_KERJA  FROM EMPLOYEES 
ORDER BY MASA_KERJA DESC</pre></code>
**Output:**
| FIRST\_NAME | LAST\_NAME | HIRE\_DATE | MASA\_KERJA |
| ----------- | ---------- | ---------- | ----------- |
| Lex         | De Haan    | 13/01/2001 | 21          |
| Daniel      | Faviet     | 16/08/2002 | 20          |
| Nancy       | Greenberg  | 17/08/2002 | 20          |
| Den         | Raphaely   | 07/12/2002 | 20          |
| Susan       | Mavris     | 07/06/2002 | 20          |
| Hermann     | Baer       | 07/06/2002 | 20          |
| Shelley     | Higgins    | 07/06/2002 | 20          |
| William     | Gietz      | 07/06/2002 | 20          |
| Steven      | King       | 17/06/2003 | 19          |
| Alexander   | Khoo       | 18/05/2003 | 19          |


# Tugas 2: Function
***

1. **Buat laporan terkait data pegawai yang berisi,**
* nama setiap pegawai, 
* jabatan, dan 
* gaji per bulan serta 
* waktu query
<pre><code>
SELECT FIRST_NAME, LAST_NAME, JOB_ID, SALARY, GETDATE() AS RUNNING_DATE FROM EMPLOYEES
</pre></code>

**Output:**
| FIRST\_NAME | LAST\_NAME | JOB\_ID     | SALARY | RUNNING\_DATE           |
| ----------- | ---------- | ----------- | ------ | ----------------------- |
| Steven      | King       | AD\_PRES    | 24000  | 2022-06-06 18:52:25.750 |
| Neena       | Kochhar    | AD\_VP      | 17000  | 2022-06-06 18:52:25.750 |
| Lex         | De Haan    | AD\_VP      | 17000  | 2022-06-06 18:52:25.750 |
| …           | …          | …           | …      | …                       |
| Hermann     | Baer       | PR\_REP     | 10000  | 2022-06-06 18:52:25.750 |
| Shelley     | Higgins    | AC\_MGR     | 12008  | 2022-06-06 18:52:25.750 |
| William     | Gietz      | AC\_ACCOUNT | 8300   | 2022-06-06 18:52:25.750 |

2. **Buat laporan terkait data pegawai yang berisi :**
* nama lengkap, 
* jabatan dan 
* bulan dipekerjakan 
khusus pegawai department STOCK (ST) dan yang bekerja/direkrut setelah tahun 2004.

<pre><code>
SELECT FIRST_NAME, LAST_NAME, JOB_ID, FORMAT (HIRE_DATE, 'MMMM yyyy') as HIRE_MONTH FROM EMPLOYEES
WHERE YEAR(HIRE_DATE)>2004
</pre></code>
**Output:**
| FIRST\_NAME | LAST\_NAME | JOB\_ID   | HIRE\_MONTH    |
| ----------- | ---------- | --------- | -------------- |
| Neena       | Kochhar    | AD\_VP    | September 2005 |
| Alexander   | Hunold     | IT\_PROG  | January 2006   |
| Bruce       | Ernst      | IT\_PROG  | May 2007       |
| …           | …          | …         | …              |
| Kevin       | Feeney     | SH\_CLERK | May 2006       |
| Donald      | OConnell   | SH\_CLERK | June 2007      |
| Douglas     | Grant      | SH\_CLERK | January 2008   |

3. **Buat laporan terkait data pegawai yang berisi :**
* ID , 
* nama setiap pegawai, 
* jabatan,
* gaji per bulan,
* lama bekerja hingga saat ini, 
dengan output sebagai berikut:

<pre><code>SELECT EMPLOYEE_ID, CONCAT(FIRST_NAME, ' ', LAST_NAME) AS NAME, JOB_ID, SALARY,
HIRE_DATE, DATEDIFF(month, HIRE_DATE , GETDATE()) AS LAMA_BEKERJA FROM EMPLOYEES  
</pre></code>

**Output:**
| EMPLOYEE\_ID | NAME            | JOB\_ID     | SALARY     | HIRE\_DATE | LAMA\_BEKERJA |
| ------------ | --------------- | ----------- | ---------- | ---------- | ------------- |
| 100          | Steven King     | AD\_PRES    | 15/09/1965 | 17/06/2003 | 228           |
| 101          | Neena Kochhar   | AD\_VP      | 17/07/1946 | 21/09/2005 | 201           |
| 102          | Lex De Haan     | AD\_VP      | 17/07/1946 | 13/01/2001 | 257           |
| …            | …               | …           | …          | …          | …             |
| 204          | Hermann Baer    | PR\_REP     | 18/05/1927 | 07/06/2002 | 240           |
| 205          | Shelley Higgins | AC\_MGR     | 15/11/1932 | 07/06/2002 | 240           |
| 206          | William Gietz   | AC\_ACCOUNT | 21/09/1922 | 07/06/2002 | 240           |
