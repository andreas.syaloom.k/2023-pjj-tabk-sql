# Hari 02 - Basic Select
***

**1. HR Department meminta laporan dengan informasi berupa nama setiap pegawai, jabatan, dan gaji per bulan**
<pre><code>SELECT FIRST_NAME, LAST_NAME ,JOB_ID, SALARY FROM EMPLOYEES</pre></code>

Output : 107 Records 
| FIRST\_NAME | LAST\_NAME | JOB\_ID     | SALARY  |
| ----------- | ---------- | ----------- | ------- |
| Steven      | King       | AD\_PRES    | 24000.0 |
| Neena       | Kochhar    | AD\_VP      | 17000.0 |
| Lex         | De Haan    | AD\_VP      | 17000.0 |
| …           | …          | …           | …       |
| Shelley     | Higgins    | AC\_MGR     | 12008.0 |
| William     | Gietz      | AC\_ACCOUNT | 8300.0  |

**2. HR Department meminta laporan dengan informasi berupa nama pegawai, jabatan, gaji per tahun, dan pajak sebesar 5% dari gaji per tahun**
<pre><code>SELECT FIRST_NAME, LAST_NAME ,JOB_ID, (SALARY*12) AS SAL_PERYEAR, (SALARY*12*5/100) AS TAX FROM EMPLOYEES </pre></code>
Output : 107 Records 
| FIRST\_NAME | LAST\_NAME | JOB\_ID     | SAL\_PERYEAR | TAX     |
| ----------- | ---------- | ----------- | ------------ | ------- |
| Steven      | King       | AD\_PRES    | 288000.0     | 14400.0 |
| Neena       | Kochhar    | AD\_VP      | 204000.0     | 10200.0 |
| Lex         | De Haan    | AD\_VP      | 204000.0     | 10200.0 |
| …           | …          | …           | …            | …       |
| Shelley     | Higgins    | AC\_MGR     | 144096.0     | 7204.8  |
| William     | Gietz      | AC\_ACCOUNT | 99600.0      | 4980.0  |

**3. HR Department meminta informasi terkait Job ID apa saja yang ada di tabel pegawai**
<pre><code>SELECT DISTINCT JOB_ID FROM EMPLOYEES</pre></code>
Output : 20 Records 
| JOB\_ID     |
| ----------- |
| AC\_ACCOUNT |
| AC\_MGR     |
| AD\_ASST    |
| …           |
| SH\_CLERK   |
| ST\_CLERK   |
| ST\_MAN     |
